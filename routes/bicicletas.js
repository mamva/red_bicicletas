var express = require('express');
var router = express.Router();
var bicicletaCrontroller = require('../controllers/bicicleta');

router.get('/', bicicletaCrontroller.Bicicleta_List);
router.get('/create', bicicletaCrontroller.Bicicleta_Create_get);
router.post('/create', bicicletaCrontroller.Bicicleta_Create_post);
router.get('/:id/update', bicicletaCrontroller.Bicicleta_Update_get);
router.post('/:id/update', bicicletaCrontroller.Bicicleta_Update_post);
router.post('/:id/delete', bicicletaCrontroller.Bicicleta_Delete_post);

module.exports = router;
