var express = require('express');
var router = express.Router();
var bicicletaCrontroller = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaCrontroller.Bicicleta_List);
router.post('/create', bicicletaCrontroller.Bicicleta_Create);
router.get('/:id/update', bicicletaCrontroller.Bicicleta_Update_Get);
router.post('/:id/update', bicicletaCrontroller.Bicicleta_Update_Post);
router.post('/delete', bicicletaCrontroller.Bicicleta_Delete);

module.exports = router;