var Bicicleta = require('../../models/bicicleta');

exports.Bicicleta_List = function(req, res){
    res.status(200).json({
            bicicletas: Bicicleta.allBicis
    });

}

exports.Bicicleta_Create = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });

}

exports.Bicicleta_Update_Get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    
    res.status(200).json({
        bicicleta: bici
    });
    
};


exports.Bicicleta_Update_Post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);

    bici.id = req.params.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng]; 
   
    res.status(200).json({
        bicicleta: bici
    });
}

exports.Bicicleta_Delete = function(req, res){
     Bicicleta.RemoveById(req.body.id)
     res.status(200).send();

}