var bicicleta = require('../../models/bicicleta');



beforeEach(() => {bicicleta.allBicis=[];});

describe('bicicleta.allBicis', () => {
    it('comienza vacia', ()=>{
           expect(bicicleta.allBicis.length).toBe(0);
    });
});

describe('bicicleta.add', ()=>{
    it('agregamos una ', ()=>{
        expect(bicicleta.allBicis.length).toBe(0);

        var a = new bicicleta(1,'Azul', 'Urbano', [14.6070454,-90.5145761]);
        bicicleta.add(a)

        expect(bicicleta.allBicis.length).toBe(1);
        expect(bicicleta.allBicis[0]).toBe(a);    

    });

});

describe('bicicleta.findById',()=>{
    it('debe devolver la bici con id 1',()=>{
        expect(bicicleta.allBicis.length).toBe(0);
        var aBici = new bicicleta(1,'verde', 'urbano');
        var aBici2 = new bicicleta(2,'rojo', 'montaña');

        bicicleta.add(aBici);
        bicicleta.add(aBici2);

        var targetBici = bicicleta.findById(1)
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    })
});