var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id,
    this.color = color,
    this.modelo = modelo,
    this.ubicacion = ubicacion

}

Bicicleta.prototype.toString = function(){
        return 'id: ' + this.id + " | color: " + this.color;
}


Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

var a = new Bicicleta(1,'Azul', 'Urbano', [14.6070454,-90.5145761]);
var b = new Bicicleta(2,'Rojo', 'Urbano', [14.5839966,-90.5245952]);

Bicicleta.add(a);
Bicicleta.add(b);

Bicicleta.findById = function(aBiciId){
      var aBici = Bicicleta.allBicis.find(x=>x.id==aBiciId)
      
      if(aBici)
        return aBici
      else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`)  

}


Bicicleta.RemoveById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break
        }   

    }
}

module.exports = Bicicleta;